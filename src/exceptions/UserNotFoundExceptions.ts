import HttpException from './HttpException.js';

class UserNotFoundException extends HttpException {
    constructor(path : string){
        super(404,`User with id : ${path} was not found!`);
    }
}

export default UserNotFoundException;