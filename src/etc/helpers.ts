import fs from 'fs/promises';
import { User } from './types';

export const saveDb = async (path : string,db : User[]) =>{
    await fs.writeFile(path,JSON.stringify(db));
}
export const generateId = () => {
    return Math.random().toString(32).slice(2);
}

export const generateReqId = () => {
    return Math.random().toString(36).slice(2);
}

export const checkIfExists = async (path:string) =>{
    try {
        await fs.access(path);
        return true;
    } catch(err){
        return false;
    }
}