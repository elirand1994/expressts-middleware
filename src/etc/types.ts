export interface User {
    id : string,
    user_name : string,
    password : number,
    email : string
}

export interface ResponseObj {
    code : number,
    message : string,
    data: any
}

export interface ErrorResponse {
    code : number,
    message: string,
    stack? : string
}





