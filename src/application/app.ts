
import express from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';
import crudRouter from '../routers/apiRouters/api.js';
import errorMiddlewares from '../middlewares/errors.js';
import  initMiddlewares from '../middlewares/initializers.js';
import {env} from "../etc/envModule.js";

class App {

    private app : express.Application;
    
    constructor(){
        // Setting up the app
        this.app = express();
        this.initializer();
    }

    private initializer = () : void => {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(morgan('dev'));
        
        // Middlewares for loading the DB and logger functions.
        this.app.use(initMiddlewares.functionLogger);
        this.app.use(initMiddlewares.loader);
        
        // API endpoint
        this.app.use('/api/users',crudRouter);

        //Middlewares for errors
        // when the url that was given wasn't found in the crudRouter, it goes to UrlNotFoundMiddleware and executes the
        // next function with, which calls the next middleware with new UrlNotFound exception as an argument
        //If a next function was called from the crudRouter itself, it searches for the first 4 param middleware
        this.app.use(errorMiddlewares.ResourceNotFoundMiddleware);
        this.app.use(errorMiddlewares.WriteToErrorLogFileMiddleware);
        this.app.use(errorMiddlewares.ErrorResponseMiddleWare);

    }

    public start = () : void => {
        this.app.listen(env.PORT, env.HOST,  ()=> {
            log.magenta(`🌎  listening on`,`http://${env.HOST}:${env.PORT}`);
        });
    }
}

const app = new App();

export default app;







