import { generateId, saveDb } from "../etc/helpers.js";
import { ResponseObj, User } from "../etc/types.js";
import * as PATH from '../etc/constants.js';
import UserNotFoundException from "../exceptions/UserNotFoundExceptions.js";

export const addUser = async (db : User[], info : any,) : Promise<ResponseObj> => {
    const {user_name,password,email} = info;
    const responseObj : User = {
        id : generateId(),
        user_name : user_name,
        password : password,
        email : email
    }
    db.push(responseObj);
    const response : ResponseObj ={
        code : 200,
        message : `User with id : ${responseObj.id} has been created!`,
        data : responseObj
    };
    await saveDb(PATH.dbPath,db);
    return response;
}

export const getAllUsers = async (db : User[]): Promise<ResponseObj> =>{
    const users = db;
    const response : ResponseObj = {
        code : 200,
        message : `All users loaded, number is : ${users.length}`,
        data : users
    }
    return response;
}

export const getUserById = async (db: User[], id : string) : Promise<ResponseObj> =>{
    const index = db.findIndex((user : User)=> user.id === id);
    if (index < 0){
        throw new UserNotFoundException(id);
    }
    const user = db[index];
    const response : ResponseObj ={
        code : 200,
        message: `User with id : ${id} has been found!`,
        data : user
    }
    return response;
}

export const updateUserById = async (db: User[],updated : User, id: string) : Promise<ResponseObj> =>{
    const index = db.findIndex((user : User)=>user.id === id);
    if (index < 0) {
        throw new UserNotFoundException(id);
    } else {
        const updatedUser = {...db[index],...updated};
        db[index] = updatedUser;
        await saveDb(PATH.dbPath,db);
        const response : ResponseObj = {
            code : 200,
            message : `User with ${id} has been updated!`,
            data: updatedUser
        }
        return response;
    }
}

export const deleteUserById = async (db : User[], id: string) =>{
    const user = db.find((user : User)=>user.id === id);
    if (!user) {
        throw new UserNotFoundException(id);
    } else {
        db = db.filter((user : User)=> user.id !== id);
        await saveDb(PATH.dbPath,db);
        const response : ResponseObj ={
            code : 200,
            message : `User with id : ${id} has been succesfully deleted!`,
            data: user
        }
        return response;
    }
}