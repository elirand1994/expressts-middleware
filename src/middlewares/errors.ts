import fs from 'fs';
import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/HttpException.js';
import UrlNotFoundExpection from '../exceptions/ResourceNotFoundException.js';
import {ErrorResponse} from '../etc/types.js';
import {errorLogPath} from '../etc/constants.js';
class ErrorMiddlewares {
    ResourceNotFoundMiddleware = async (req : Request, res: Response, next : NextFunction) =>{
        next(new UrlNotFoundExpection(req.url));
    }

    ErrorResponseMiddleWare = async (err: HttpException, req : Request, res : Response, next : NextFunction) =>{
        const response : ErrorResponse = {
            code : err.status,
            message : err.message,
            stack : err.stack
        }
        res.status(response.code).json(response);
    }

    WriteToErrorLogFileMiddleware = ()=>{
        const errorsFileLogger = fs.createWriteStream(errorLogPath, { flags: 'a' });
        return (error: HttpException, req: Request, res: Response, next: NextFunction) => {
            errorsFileLogger.write(`${req.id} :: ${error.status} :: ${error.message} >> ${error.stack} \n`);
            next(error);
        }
    }
}

const errorMiddlewares = new ErrorMiddlewares();
export default errorMiddlewares;


