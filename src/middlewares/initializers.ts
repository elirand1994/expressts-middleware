
import fs from 'fs/promises';
import{ NextFunction,Request,Response } from 'express';
import {checkIfExists,generateReqId} from '../etc/helpers.js';
import log from '@ajar/marker';
import { User } from '../etc/types.js';
import * as PATH from '../etc/constants.js';

class InitializerMiddlewares {
    functionLogger  =  async (req :Request,res :Response, next : NextFunction) =>{
        log.cyan('Writing to the log...')
        const flag = await checkIfExists(PATH.logPath);
        if (!flag){
            console.log('Creating log file...')
            await fs.writeFile(PATH.logPath,JSON.stringify([]),'utf-8');
        }
        const content = req.method + ' ' + req.originalUrl + ' ' + Date.now() + '' + '\r\n';
        await fs.appendFile(PATH.logPath,content);
        log.green('Completed writing to log!');
        next();
    }

    loader = async (req:Request,res:Response,next : NextFunction) =>{
        const flag = await checkIfExists(PATH.dbPath);
        if (!flag){
            console.log('Creating db file...')
            await fs.writeFile(PATH.dbPath,JSON.stringify([]),'utf-8');
        }
        const json = await fs.readFile(PATH.dbPath,'utf-8');
        const users = await JSON.parse(json) as User[];
        req.users = users;
        req.id = generateReqId();
        next();
    }
}

const initMiddlewares = new InitializerMiddlewares();
export default initMiddlewares;


