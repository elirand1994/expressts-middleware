
import express from 'express';
import controller from '../../controller/controller.js';

const router = express.Router();

router.post('/',controller.addUser);
router.get('/',controller.getAllUsers);
router.get('/:id',controller.getUser);
router.patch('/:id',controller.updateUser);
router.delete('/:id',controller.deleteUser);

export default router;