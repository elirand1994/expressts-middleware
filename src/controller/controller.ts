import { User } from '../etc/types.js';
import { NextFunction, Request, Response} from 'express';
import * as CORE from '../core/core.js';
class Controller {

    addUser = async (req:Request, res : Response, next : NextFunction) =>{
        try {
            const response = await CORE.addUser(req.users,req.body);
            res.status(response.code).json(response);
        } catch (err) {
            next(err);
        }
    }

    getAllUsers = async (req:Request, res : Response, next : NextFunction) =>{
        try {
            const response = await CORE.getAllUsers(req.users);
            res.status(response.code).json(response);
        }catch(err){
            next(err)
        }
    }

    getUser = async (req:Request, res : Response, next : NextFunction) =>{
        try {
            const response = await CORE.getUserById(req.users,req.params.id);
            res.status(response.code).json(response);
        } catch (err){
            next(err);
        }
    }

    updateUser = async (req:Request, res : Response, next : NextFunction) =>{
        try {
            const response = await CORE.updateUserById(req.users,req.body as User,req.params.id);
            res.status(response.code).json(response);
        } catch(err){
            next(err);
        }
    }

    deleteUser = async (req:Request, res : Response, next : NextFunction) =>{
        try {
            const response = await CORE.deleteUserById(req.users,req.params.id);
            res.status(response.code).json(response);
        } catch(err){
            next(err);
        }
    }
}

const controller = new Controller();
export default controller;